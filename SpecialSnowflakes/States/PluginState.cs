﻿using Dalamud.Plugin.Services;
using Dalamud.Game.ClientState.Objects.SubKinds;
using System;
using Dalamud.Hooking;
using Dalamud.Logging;
using System.Diagnostics;

namespace SpecialSnowflakes.States
{
    public unsafe class CameraSettings
    {
        public static float CameraZoomDelta = 0.75f;
        public static char CameraCollisionState = (char)0;
        public IntPtr CameraManagerAddress { get; set; }
        public IntPtr* BaseCameraTable { get; set; }
        public CameraSettings()
        {
        }
    }
    internal class PluginState
    {
        private IClientState ClientState { get; init; }
        private IGameInteropProvider GameInteropProvider { get; init; }

        private delegate float getCameraZoomDeltaDelegate();
        private static Hook<getCameraZoomDeltaDelegate> getCameraZoomDeltaHook;
        private static float getCameraZoomDeltaDetour() => CameraSettings.CameraZoomDelta;

        public bool InDutyOrCombat { get; set; }
        public bool Mounted { get; set; }
        public CameraSettings CameraSettings { get; set; }
        public PluginState(IClientState clientState, IGameInteropProvider gameInteropProvider)
        {
            ClientState = clientState;
            GameInteropProvider = gameInteropProvider;
            CameraSettings = new CameraSettings();
        }
        public IPlayerCharacter GetLocalPlayer()
        {
            return ClientState.LocalPlayer;
        }

        public bool IsLoggedIn()
        {
            return ClientState.IsLoggedIn;
        }

        public unsafe void SetCameraHookState(bool allowed)
        {
            if (getCameraZoomDeltaHook != null && !getCameraZoomDeltaHook.IsDisposed)
            {
                getCameraZoomDeltaHook.Disable();
                getCameraZoomDeltaHook.Dispose();
            }
            if (allowed)
            {
                getCameraZoomDeltaHook = GameInteropProvider.HookFromAddress<getCameraZoomDeltaDelegate>(CameraSettings.BaseCameraTable[28], getCameraZoomDeltaDetour);
                getCameraZoomDeltaHook.Enable();
            }
        }

        public void Dispose()
        {
            if (getCameraZoomDeltaHook != null)
            {
                getCameraZoomDeltaHook.Disable();
                getCameraZoomDeltaHook.Dispose();
            }
        }

    }
}
