﻿using Dalamud.Game;
using Dalamud.Game.ClientState.Conditions;
using Dalamud.Plugin.Services;
using System;

using SpecialSnowflakes.Chara;
using SpecialSnowflakes.UI;
using SpecialSnowflakes.Utils;
using SpecialSnowflakes.States;
using SpecialSnowflakes.Extras;
using System.Text.RegularExpressions;

namespace SpecialSnowflakes
{
    public class PluginPreferences
    {
        public static bool SwapExtrasAndMain = false;

        public static bool AllowZoomAdjustments = false;
        public static bool DisllowDutyZoomAdjustments = false;
        public static bool DisallowGPoseZoomAdjustments = false;
        public static bool DisallowMountedZoomAdjustments = false;

        public static bool FogOverride = false;

        public static bool CharacterDelimit = false;
        public PluginPreferences(Configuration config)
        {
            AllowZoomAdjustments = config.zoomScaling;
            DisllowDutyZoomAdjustments = config.allowDutyDynamicZoom;
            DisallowGPoseZoomAdjustments = config.disableGPoseDynamicZoom;
            DisallowMountedZoomAdjustments = config.disableMountDynamicZoom;
            SwapExtrasAndMain = config.swapExtrasMain;
        }
    }
    internal class SpecialSnowflakes
    {
        // Constants
        private const int CHARACTER_SEARCH_TIMEOUT = 5000;

        // Dalamud
        private IClientState clientState { get; init; }
        private ICondition condition { get; init; }
        private Configuration configuration { get; init; }
        private IFramework framework { get; init; }
        private IGameInteropProvider gameInteropProvider { get; init; }
        private ISigScanner sigScanner { get; init; }
        private IChatGui chatGui { get; init; }

        // Static Dalamud Interfaces
        public static IPluginLog PluginLogger { get; set; } = null!;

        private PluginState PluginState { get; init; }
        private PluginUI PluginUi { get; init; }

        // Main Controller
        private PluginPreferences pluginPreferences { get; init; }
        private SSFCharacterManager characterManager { get; init; }
        private CharacterRefreshManager characterRefreshManager { get; init; }

        // Extras
        public FogRemoval FogRemoval { get; init; }
        public CharacterDelimit CharacterDelimit { get; init; }

        public SpecialSnowflakes(
            IClientState clientState,
            ICondition condition,
            IFramework framework,
            IChatGui chatGui,
            IObjectTable objectTable,
            IGameInteropProvider gameInteropProvider,
            ISigScanner sigScanner,
            IPluginLog pluginLogger,
            Configuration config)
        {
            this.clientState = clientState;
            this.condition = condition;
            this.framework = framework;
            this.chatGui = chatGui;
            this.sigScanner = sigScanner;
            this.gameInteropProvider = gameInteropProvider;
            configuration = config;

            PluginLogger = pluginLogger;

            PluginState = new PluginState(clientState, gameInteropProvider);
            pluginPreferences = new PluginPreferences(configuration);
            initCameraSettings();
            characterManager = new SSFCharacterManager(configuration, objectTable);
            characterRefreshManager = new CharacterRefreshManager(configuration, characterManager, PluginState);

            PluginUi = new PluginUI(this, configuration, characterManager, characterRefreshManager);

            FogRemoval = new FogRemoval(this.sigScanner, gameInteropProvider);
            CharacterDelimit = new CharacterDelimit(this.sigScanner);

            framework.Update += OnUpdateEvent;
            this.clientState.Logout += onLogoutEvent;
        }

        private void onLogoutEvent(int type, int code)
        {
            PluginLogger.Debug("Logout detected, clearing cached addresses.");
            characterManager.ReinitCharacterAddresses();
        }

        public void OnUpdateEvent(object framework)
        {
            if (condition[ConditionFlag.BetweenAreas51]
             || condition[ConditionFlag.BetweenAreas]
             || condition[ConditionFlag.OccupiedInCutSceneEvent])
            {
                characterRefreshManager.ResetCameraDefaults();
                return;
            }
            if (PluginPreferences.AllowZoomAdjustments)
            {
                if (PluginPreferences.DisllowDutyZoomAdjustments)
                {
                    if (condition[ConditionFlag.InCombat] ||
                        condition[ConditionFlag.BoundByDuty])
                    {
                        if (PluginState.InDutyOrCombat == false)
                        {
                            PluginState.InDutyOrCombat = true;
                            characterRefreshManager.ResetCameraDefaults();
                        }
                    }
                    else
                    {
                        PluginState.InDutyOrCombat = false;
                    }
                }
                if (PluginPreferences.DisallowMountedZoomAdjustments)
                {
                    if (condition[ConditionFlag.Mounted] ||
                        condition[ConditionFlag.Mounted2])
                    {
                        if (PluginState.Mounted == false)
                        {
                            PluginState.Mounted = true;
                            characterRefreshManager.ResetCameraDefaults();
                        }
                    }
                    else
                    {
                        PluginState.Mounted = false;
                    }
                }
            }
            if (characterRefreshManager.CurrentRefreshState.CurrentMode != RefreshMode.ALWAYS) return;
            if (characterManager.IsCharacterQueueEmpty()) characterManager.InitQueue();
            characterRefreshManager.RefreshNextHeight();
        }
        private unsafe void initCameraSettings()
        {
            byte* tempAddr;
            IntPtr cameraManagerSignatureAddr;
            if (sigScanner.TryGetStaticAddressFromSig("48 8D 35 ?? ?? ?? ?? 48 8B 09", out cameraManagerSignatureAddr, 0) == false)
            {
                PluginLogger.Debug($"Camera manager could not be found. Disabling hook.");
                return;
            }
            Buffer.MemoryCopy(cameraManagerSignatureAddr.ToPointer(), &tempAddr, sizeof(byte*), sizeof(byte*));
            PluginState.CameraSettings.CameraManagerAddress = new IntPtr(tempAddr);
            IntPtr* baseCameraTable;
            Buffer.MemoryCopy(PluginState.CameraSettings.CameraManagerAddress.ToPointer(), &baseCameraTable, sizeof(IntPtr*), sizeof(IntPtr*));
            PluginState.CameraSettings.BaseCameraTable = baseCameraTable;
            PluginState.SetCameraHookState(PluginPreferences.AllowZoomAdjustments);
        }

        public void DrawUI()
        {
            PluginUi.Draw();
        }

        public void DrawMainUI()
        {
            PluginUi.Visible = true;
        }
        public void DrawConfigUI()
        {
            PluginUi.PreferencesVisible = true;
        }

        public void OnCommand(string command, string args)
        {
            switch (args)
            {
                case "off":
                    characterRefreshManager.CurrentRefreshState.CurrentMode = RefreshMode.DISABLED;
                    characterRefreshManager.ResetCameraDefaults();
                    PluginUi.stopRefreshCommand();
                    break;
                case "interval":
                    characterRefreshManager.CurrentRefreshState.CurrentMode = RefreshMode.INTERVAL;
                    PluginUi.stopRefreshCommand();
                    break;
                case "on":
                case "always":
                    characterRefreshManager.CurrentRefreshState.CurrentMode = RefreshMode.ALWAYS;
                    PluginUi.stopRefreshCommand();
                    break;
                case "refresh":
                    characterRefreshManager.RefreshAllHeights();
                    break;
                case "extras":
                    PluginUi.ExtrasVisible =! PluginUi.ExtrasVisible;
                    break;
                case "fog off":
                    FogRemoval.disableFog();
                    break;
                case "fog on":
                    FogRemoval.enableFog();
                    break;
                case "renderlimit off":
                case "rlimit off":
                    CharacterDelimit.disableLimit();
                    break;
                case "renderlimit on":
                case "rlimit on":
                    CharacterDelimit.enableLimit();
                    break;
                case "camera off":
                    PluginPreferences.AllowZoomAdjustments = false;
                    configuration.zoomScaling = PluginPreferences.AllowZoomAdjustments;
                    configuration.Save();
                    characterRefreshManager.ResetCameraDefaults();
                    break;
                case "camera on":
                    PluginPreferences.AllowZoomAdjustments = true;
                    configuration.zoomScaling = PluginPreferences.AllowZoomAdjustments;
                    configuration.Save();
                    characterRefreshManager.ResetCameraDefaults();
                    break;
                case string x when (x == "height" || x.StartsWith("height ")):
                    Match m = Regex.Match(x, "(?:height )(\\S+ \\S+) (\\d+(?:\\.\\d*)?|\\.\\d+)");
                    if (m.Success)
                    {
                        Character? tempCharacter = characterManager.CreateCharacter(m.Groups[1].Value);
                        if (tempCharacter == null)
                        {
                            chatGui.Print($"Could not find player with the name {m.Groups[1].Value} nearby.", "SSF", 57);
                            break;
                        }
                        if (characterManager.CharacterExists(tempCharacter))
                        {
                            Character? matchedChatacter = characterManager.Characters.Find(c => c.Name == tempCharacter.Name);
                            if (matchedChatacter != null && matchedChatacter.ReadOnlyHeight == false)
                            {
                                matchedChatacter.ReadOnlyHeight = true;
                                chatGui.Print($"Disabled auto refresh for {m.Groups[1].Value}.", "SSF", 57);
                            }
                        }
                        unsafe
                        {
                            characterRefreshManager.UpdateCharacterBase(tempCharacter);
                        }
                        characterRefreshManager.SetTemporaryHeight(tempCharacter, float.Parse(m.Groups[2].Value));
                    } else
                    {
                        chatGui.Print($"Error in formatting.", "SSF", 57);
                    }
                    break;
                default:
                    if (PluginPreferences.SwapExtrasAndMain) PluginUi.ExtrasVisible = !PluginUi.ExtrasVisible;
                    else PluginUi.Visible = !PluginUi.Visible;
                    break;
            }
        }

        public void Dispose()
        {
            framework.Update -= OnUpdateEvent;
            clientState.Logout -= onLogoutEvent;
            characterRefreshManager.Dispose();
            PluginState.Dispose();
            CharacterDelimit.Dispose();
            FogRemoval.Dispose();
        }
    }
}
