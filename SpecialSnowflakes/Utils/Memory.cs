﻿using System;

namespace SpecialSnowflakes.Utils
{
    internal static class MemoryManager
    {
        private const int MODEL_VIEW_ADDR_OFFSET = 0x0100;
        public static unsafe byte* GetModelView(byte* characterBase)
        {
            byte* modelView = null;
            Buffer.MemoryCopy(characterBase + MODEL_VIEW_ADDR_OFFSET, &modelView, sizeof(byte*), sizeof(byte*));
            return modelView;
        }
        public static unsafe void ReadFloatValue(byte* baseAddr, int offset, float* floatPointer)
        {
            Buffer.MemoryCopy(baseAddr + offset, floatPointer, sizeof(float), sizeof(float));
        }
        public static unsafe void WriteFloatValue(float* floatPointer, byte* baseAddr, int offset)
        {
            Buffer.MemoryCopy(floatPointer, baseAddr + offset, sizeof(float), sizeof(float));
        }
    }

}
