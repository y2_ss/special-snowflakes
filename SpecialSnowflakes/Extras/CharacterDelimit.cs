﻿using Dalamud.Game;
using Dalamud.Memory;
using System;

namespace SpecialSnowflakes.Extras
{
    internal unsafe class CharacterDelimit
    {

        private ISigScanner SigScanner;

        // Float comparison to defined 1024.0 limit. Final 4 bytes may change from patch to patch.
        private string LIMIT_COMPARE_SIG = "0F 2F 35 ?? ?? 9E 01";
        private int JUMP_OFFSET = 0x7;
        
        // Replace JNA with unconditional JMP for now. 
        private byte[] JMP_ARRAY = { 0xEB };
        private byte[] JNA_ARRAY = { 0x76 };

        private IntPtr limitCompareAddress;

        public unsafe CharacterDelimit(
            ISigScanner sigScanner
        )
        {
            SigScanner = sigScanner;
        }

        public unsafe void disableLimit()
        {
            // Note: Kinda hacky, will look into how to make this reliable.
            limitCompareAddress = new IntPtr((byte*)SigScanner.ScanText(LIMIT_COMPARE_SIG) + JUMP_OFFSET);

            if ( limitCompareAddress == IntPtr.Zero ) return;

            MemoryProtection oldMemoryPermissions;
            MemoryHelper.ChangePermission(limitCompareAddress, sizeof(byte) * 2, MemoryProtection.ExecuteReadWrite, out oldMemoryPermissions);
            MemoryHelper.Write<byte>(limitCompareAddress, JMP_ARRAY);
            MemoryHelper.ChangePermission(limitCompareAddress, sizeof(byte) * 2, oldMemoryPermissions);

            PluginPreferences.CharacterDelimit = true;
            return;
        }

        public unsafe void enableLimit()
        {
            // Note: Kinda hacky, will look into how to make this reliable.
            limitCompareAddress = new IntPtr((byte*)SigScanner.ScanText(LIMIT_COMPARE_SIG) + JUMP_OFFSET);

            if (limitCompareAddress == IntPtr.Zero) return;

            MemoryProtection oldMemoryPermissions;
            MemoryHelper.ChangePermission(limitCompareAddress, sizeof(byte), MemoryProtection.ExecuteReadWrite, out oldMemoryPermissions);
            MemoryHelper.Write<byte>(limitCompareAddress, JNA_ARRAY);
            MemoryHelper.ChangePermission(limitCompareAddress, sizeof(byte), oldMemoryPermissions);

            PluginPreferences.CharacterDelimit = false;
            return;
        }

        public void Dispose()
        {
            if (PluginPreferences.CharacterDelimit == true)
            {
                enableLimit();
            }
        }
    }
}
