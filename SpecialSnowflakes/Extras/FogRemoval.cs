﻿using Dalamud.Game;
using SpecialSnowflakes.Utils;
using System;
using Dalamud.Hooking;
using System.Numerics;
using Dalamud.Plugin.Services;
using FFXIVClientStructs.FFXIV.Client.Graphics.Environment;
using System.Runtime.InteropServices;

// Environment editing ported from Ktisis testing beta, Thanks chirpxiv!
namespace SpecialSnowflakes.Extras
{
    [StructLayout(LayoutKind.Explicit, Size = 0x910)]
    public struct EnvironmentParameters
    {
        [FieldOffset(0x000)] public EnvManager _base;
        [FieldOffset(0x058)] public EnvironmentState EnvState;
        [FieldOffset(0x410)] public EnvSimulator EnvSimulator;
        public unsafe static EnvironmentParameters* Instance() => (EnvironmentParameters*)EnvManager.Instance();
    }

    [StructLayout(LayoutKind.Explicit, Size = 0x2F8)]
    public struct EnvironmentState
    {
        [FieldOffset(0x020)] public EnvLighting Lighting;
        [FieldOffset(0x0C0)] public EnvFog Fog;
    }
    [StructLayout(LayoutKind.Explicit, Size = 0x28)]
    public struct EnvFog
    {
        [FieldOffset(0x00)] public Vector4 Color;
        [FieldOffset(0x10)] public float Distance;
        [FieldOffset(0x14)] public float Thickness;
        [FieldOffset(0x18)] public float _unk1;
        [FieldOffset(0x1C)] public float _unk2;
        [FieldOffset(0x20)] public float Opacity;
        [FieldOffset(0x24)] public float SkyVisibility;
    }
    [StructLayout(LayoutKind.Explicit, Size = 0x40)]
    public struct EnvLighting
    {
        [FieldOffset(0x00)] public Vector3 SunLightColor;
        [FieldOffset(0x0C)] public Vector3 MoonLightColor;
        [FieldOffset(0x18)] public Vector3 Ambient;
        [FieldOffset(0x24)] public float _unk1;
        [FieldOffset(0x28)] public float AmbientSaturation;
        [FieldOffset(0x2C)] public float Temperature;
        [FieldOffset(0x30)] public float _unk2;
        [FieldOffset(0x34)] public float _unk3;
        [FieldOffset(0x38)] public float _unk4;
    }
    internal unsafe class FogRemoval
    {
        private unsafe delegate nint EnvironmentUpdateDelegate(EnvironmentParameters* env, float a2, float a3);
        private static Hook<EnvironmentUpdateDelegate> EnvironmentUpdateHook = null!;
        private unsafe delegate nint EnvironmentWriterDelegate(EnvironmentState* dest, EnvironmentState* src);
        private static Hook<EnvironmentWriterDelegate> EnvironmentWriterHook = null!;
        private unsafe static nint EnvironmentUpdateDetour(EnvironmentParameters* env, float a2, float a3)
        {
            return EnvironmentUpdateHook.Original(env, a2, a3);
        }
        private unsafe nint EnvironmentWriterDetour(EnvironmentState* dest, EnvironmentState* src)
        {
            EnvironmentState? original = null;
            if (PluginPreferences.FogOverride == true)
                original = *dest;
            var exec = EnvironmentWriterHook.Original(dest, src);
            if (original != null)
            {
                dest->Fog = original.Value.Fog;
                dest->Lighting = original.Value.Lighting;
            }
            return exec;
        }

        public unsafe FogRemoval(
            ISigScanner sigScanner,
            IGameInteropProvider gameInteropProvider
        )
        {
            var environmentWriter = sigScanner.ScanText("E8 ?? ?? ?? ?? 49 3B F5 75 0D");
            EnvironmentWriterHook = gameInteropProvider.HookFromAddress<EnvironmentWriterDelegate>(environmentWriter, EnvironmentWriterDetour);

            var environmentBase = sigScanner.ScanText("E8 ?? ?? ?? ?? 48 8B 0D ?? ?? ?? ?? 41 0F 28 CA");
            EnvironmentUpdateHook = gameInteropProvider.HookFromAddress<EnvironmentUpdateDelegate>(environmentBase, EnvironmentUpdateDetour);
        }

        public void disableFog()
        {
            EnvironmentUpdateHook.Enable();
            EnvironmentWriterHook.Enable();
            PluginPreferences.FogOverride = true;
        }
        public unsafe void enableFog()
        {
            EnvironmentUpdateHook.Disable();
            EnvironmentWriterHook.Disable();
            PluginPreferences.FogOverride = false;
        }

        public void Dispose()
        {
            EnvironmentUpdateHook.Disable();
            EnvironmentUpdateHook.Dispose();
            EnvironmentWriterHook.Disable();
            EnvironmentWriterHook.Dispose();
        }
    }
}
