﻿using Dalamud.Game.ClientState.Objects;
using Dalamud.Game.ClientState.Objects.Enums;
using Dalamud.Game.ClientState.Objects.SubKinds;
using Dalamud.Game.ClientState.Objects.Types;
using Dalamud.Plugin.Services;
using Dalamud.Logging;
using SpecialSnowflakes.Utils;
using System.Collections.Generic;
using FFXIVClientStructs.FFXIV.Client.Graphics.Scene;

namespace SpecialSnowflakes.Chara
{
    internal class SSFCharacterManager
    {
        private const int BASE_SCALE_ADDR_OFFSET = 0x2A4;
        public List<Character> Characters { get; set; }
        public IObjectTable ObjectTable { get; init; }
        public Queue<Character> CharacterQueue { get; set; }
        public SSFCharacterManager(Configuration config, IObjectTable objctTable)
        {
            Characters = config.characterList;
            ObjectTable = objctTable;
            CharacterQueue = new Queue<Character>();
            ReinitCharacterAddresses();
        }
        public void ReinitCharacterAddresses()
        {
            foreach (Character character in Characters)
            {
                character.ClearCharacterAddresses();
            }
        }
        public unsafe bool AddCharacter(string characterName)
        {
            if (CharacterExists(new Character(characterName, 0.0f)))
            {
                SpecialSnowflakes.PluginLogger.Debug($"Already exists {characterName}");
                return false;
            }
            Character? c = CreateCharacter(characterName);
            if (c == null) return false;
            Characters.Add(c);
            return true;
        }
        public unsafe Character? CreateCharacter(string characterName)
        {
            float foundHeight = 1.0f;
            byte* characterBase = null;
            SpecialSnowflakes.PluginLogger.Debug($"Searching character to add... [{characterName}]");
            foreach (IGameObject obj in ObjectTable)
            {
                if (obj.Name.ToString().Equals(characterName))
                {
                    characterBase = (byte*)obj.Address.ToPointer();
                    break;
                }
            }
            if (characterBase == null) return null;
            byte* modelView = MemoryManager.GetModelView(characterBase);
            MemoryManager.ReadFloatValue(modelView, BASE_SCALE_ADDR_OFFSET, &foundHeight);
            return new Character(characterName, foundHeight);
        }
        public unsafe bool RemoveCharacter(string characterName)
        {
            Characters.Remove(new Character(characterName, 0.0f));
            return true;
        }
        public bool IsCharacterQueueEmpty()
        {
            return CharacterQueue.Count < 1;
        }
        public bool CharacterExists(Character c)
        {
            if (Characters.Contains(c))
            {
                return true;
            }
            return false;
        }
        public void InitQueue()
        {
            CharacterQueue = new Queue<Character>(Characters);
        }
        public float GetRacialModifier(Character character)
        {
            foreach (IGameObject obj in ObjectTable)
            {
                if (obj.Name.ToString().Equals(character.Name) && obj.ObjectKind == ObjectKind.Player)
                {
                    byte race = ((IPlayerCharacter)obj).Customize[(int)CustomizeIndex.Race];
                    byte gender = ((IPlayerCharacter)obj).Customize[(int)CustomizeIndex.Gender];
                    switch (race, gender)
                    {
                        case (5, 1):
                            return 0.525f;
                        case (6, 0):
                            return 0.55f;
                        case (8, 1):
                            return 0.55f;
                        default:
                            return 0.5f;
                    }
                }
            }
            return 0.5f;
        }
    }
}
