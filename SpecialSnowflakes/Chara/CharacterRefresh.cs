﻿using Dalamud.Game.ClientState.Objects.Enums;
using Dalamud.Game.ClientState.Objects.Types;
using Dalamud.Logging;
using SpecialSnowflakes.States;
using SpecialSnowflakes.Utils;
using System;
using System.Numerics;
using System.Timers;

using ClientStructCharacter = FFXIVClientStructs.FFXIV.Client.Game.Character.Character;

namespace SpecialSnowflakes.Chara
{
    public enum RefreshMode : int
    {
        DISABLED = 0,
        INTERVAL = 1,
        ALWAYS = 2,
    };
    public class RefreshState
    {
        public RefreshMode CurrentMode { get; set; }
        public int IntervalRate = 5;
        public RefreshState(Configuration config)
        {
            CurrentMode = config.refreshBoolOption;
            IntervalRate = config.refreshIntervallUpdate;
        }
    }
    internal class CharacterRefreshManager : IDisposable
    {
        private const int BASE_SCALE_ADDR_OFFSET = 0x2A4;
        private const int BASE_CUSTOM_SCALE_ADDR_OFFSET = 0x074;
        private const int CAM_ZOOM_MIN_ADDR_OFFSET = 0x118;
        private const int CAM_ZOOM_MAX_ADDR_OFFSET = 0x11C;
        private const int CAM_Y_ANGLE_ADDR_OFFSET = 0x224;

        public RefreshState CurrentRefreshState { get; init; }

        private SSFCharacterManager characterManager { get; init; }
        private PluginState pluginState { get; init; }

        private System.Timers.Timer refreshTimer { get; set; }

        public CharacterRefreshManager(Configuration config, SSFCharacterManager characterManager, PluginState pluginState)
        {
            this.pluginState = pluginState;
            this.characterManager = characterManager;

            CurrentRefreshState = new RefreshState(config);
            refreshTimer = new System.Timers.Timer();

            RefreshAllHeights();
            RestartRefresh();
        }

        // Logic for refreshing heights.
        public void RestartRefresh()
        {
            refreshTimer.Stop();
            if (CurrentRefreshState.CurrentMode != RefreshMode.INTERVAL || CurrentRefreshState.IntervalRate < 1) return;
            SpecialSnowflakes.PluginLogger.Debug($"Restarting auto-refresh with {CurrentRefreshState.IntervalRate} seconds.");
            refreshTimer = new System.Timers.Timer();
            refreshTimer.Elapsed += new ElapsedEventHandler(RefreshTrigger);
            refreshTimer.Interval = 1000 * CurrentRefreshState.IntervalRate;
            refreshTimer.Start();
        }
        public void RefreshTrigger(object source, ElapsedEventArgs e)
        {
            RefreshAllHeights();
        }
        public unsafe void RefreshNextHeight()
        {
            if (!pluginState.IsLoggedIn()) return;
            if (characterManager.IsCharacterQueueEmpty()) return;

            Character character = characterManager.CharacterQueue.Dequeue();
            UpdateCharacterBase(character);
            if (!character.ReadOnlyHeight) UpdateHeight(character);
            UpdateCameraHeight(character);
            characterManager.CharacterQueue.Enqueue(character);
        }
        public unsafe void RefreshAllHeights(bool readOnly = false)
        {
            if (!pluginState.IsLoggedIn()) return;
            foreach (Character character in characterManager.Characters)
            {
                UpdateCharacterBase(character);
                if (!readOnly) UpdateHeight(character);
                UpdateCameraHeight(character);
            }
        }
        public void StopRefresh()
        {
            if (CurrentRefreshState.CurrentMode == RefreshMode.INTERVAL)
            {
                RestartRefresh();
            }
            else
            {
                refreshTimer.Stop();
            }
        }

        // Logic for character updating, might break out.
        public unsafe byte* UpdateCharacterBase(Character character)
        {
            byte* characterBase = null;
            bool inGpose = characterManager.ObjectTable.GetObjectAddress(201) != IntPtr.Zero;

            foreach (IGameObject obj in characterManager.ObjectTable)
            {
                if (inGpose && obj.ObjectIndex < 201 || !inGpose && obj.ObjectIndex > 200)
                {
                    continue;
                }
                if (obj.ObjectKind == ObjectKind.Player && obj.Name.ToString().Equals(character.Name))
                {
                    characterBase = (byte*)obj.Address.ToPointer();
                    character.UpdateLastKnownCharacter((ClientStructCharacter*)obj.Address);
                }
            }
            return characterBase !=  null ? character.UpdateLastKnownAddresses(characterBase) : null;
        }
        public unsafe float GetCurrentHeight(byte* modelViewBaseAddr)
        {
            float currentHeight = 1.0f;
            MemoryManager.ReadFloatValue(modelViewBaseAddr, BASE_SCALE_ADDR_OFFSET, &currentHeight);
            return currentHeight;
        }
        public unsafe float GetCustomScale(byte* modelViewBaseAddr)
        {
            float currentScale = 1.0f;
            MemoryManager.ReadFloatValue(modelViewBaseAddr, BASE_CUSTOM_SCALE_ADDR_OFFSET, &currentScale);
            return currentScale;
        }

        public unsafe void UpdateHeight(Character character)
        {
            if (character.Height <= 0) return;

            float storedHeight = character.Height;

            byte* characterBaseAddr = character.GetLastKnownCharacterBase();
            byte* modelViewAddr = character.GetLastKnownModelView();
            if (characterBaseAddr == null || modelViewAddr == null)
            {
                UpdateCharacterBase(character);
                return;
            }

            if (storedHeight == GetCurrentHeight(modelViewAddr)) return;

            MemoryManager.WriteFloatValue(&storedHeight, modelViewAddr, BASE_SCALE_ADDR_OFFSET);
        }

        public unsafe void SetTemporaryHeight(Character character, float height)
        {
            if (height <= 0) return;

            float storedHeight = height;

            byte* characterBaseAddr = character.GetLastKnownCharacterBase();
            byte* modelViewAddr = character.GetLastKnownModelView();
            if (characterBaseAddr == null || modelViewAddr == null)
            {
                UpdateCharacterBase(character);
                return;
            }

            if (storedHeight == GetCurrentHeight(modelViewAddr)) return;

            MemoryManager.WriteFloatValue(&storedHeight, modelViewAddr, BASE_SCALE_ADDR_OFFSET);
        }

        public unsafe void UpdateCameraHeight(Character character)
        {
            if (pluginState.GetLocalPlayer() == null || !pluginState.GetLocalPlayer().Name.ToString().Equals(character.Name))
            {
                return;
            }
            bool inGpose = characterManager.ObjectTable.GetObjectAddress(201) != IntPtr.Zero;
            if (PluginPreferences.AllowZoomAdjustments
                && !(PluginPreferences.DisllowDutyZoomAdjustments && pluginState.InDutyOrCombat)
                && !(PluginPreferences.DisallowMountedZoomAdjustments && pluginState.Mounted)
                && !(PluginPreferences.DisallowGPoseZoomAdjustments && inGpose)
            )
            {
                byte* characterBase = character.GetLastKnownCharacterBase();
                byte* modelView = character.GetLastKnownModelView();
                if (characterBase == null || modelView == null || character.GetLastKnownCharacter() == null)
                {
                    UpdateCharacterBase(character);
                    return;
                }
                float currentHeight = character.ReadOnlyHeight ? GetCurrentHeight(modelView) : character.Height;
                float currentScale = character.DisableCustomScale ? 1.0f : GetCustomScale(modelView);

                if (currentHeight == 0) return;

                if (CameraSettings.CameraZoomDelta == 0.75f * currentHeight * currentScale)
                {
                    return; //easy way to tell if height changed;
                }

                SpecialSnowflakes.PluginLogger.Debug("Updating camera");

                CameraSettings.CameraZoomDelta = 0.75f * currentHeight * currentScale;
                float cameraZoomMin = 1.5f * currentHeight * currentScale;
                float cameraZoomMax = 20f * currentHeight * currentScale;
                float cameraUpDown = -0.22867f * currentHeight;


                if (pluginState.CameraSettings.CameraManagerAddress != IntPtr.Zero)
                {

                    MemoryManager.WriteFloatValue(&cameraZoomMin, (byte*)pluginState.CameraSettings.CameraManagerAddress.ToPointer(), CAM_ZOOM_MIN_ADDR_OFFSET);
                    MemoryManager.WriteFloatValue(&cameraZoomMax, (byte*)pluginState.CameraSettings.CameraManagerAddress.ToPointer(), CAM_ZOOM_MAX_ADDR_OFFSET);

                    /*
                    float yCamAdjust = 0.0f;
                    float* yCamAdjustAddr = &yCamAdjust;
                    Buffer.MemoryCopy(characterBase + 0x184, yCamAdjustAddr, sizeof(float), sizeof(float));
                    PluginLog.Debug($"Y value: {yCamAdjust}");

                    yCamAdjust *= currentScale;
                    Buffer.MemoryCopy(&yCamAdjust, characterBase + 0x184, sizeof(float), sizeof(float));
                    */

                    MemoryManager.WriteFloatValue(&cameraUpDown, (byte*)pluginState.CameraSettings.CameraManagerAddress.ToPointer(), CAM_Y_ANGLE_ADDR_OFFSET);
                    
                }
            }
            else if (inGpose && PluginPreferences.DisallowGPoseZoomAdjustments)
            {
                ResetCameraDefaults();
            }
        }
        public unsafe void ResetCameraDefaults()
        {
            if (CameraSettings.CameraZoomDelta != 0.75f)
            {
                CameraSettings.CameraZoomDelta = 0.75f;
                pluginState.SetCameraHookState(PluginPreferences.AllowZoomAdjustments);
            } else
            {
                return;
            }

            // CAMERA DEFAULTS
            float cameraZoomMin = 1.5f;
            float cameraZoomMax = 20f;
            float cameraUpDown = -0.21952191f;

            if (pluginState.CameraSettings.CameraManagerAddress != IntPtr.Zero)
            {
                MemoryManager.WriteFloatValue(&cameraZoomMin, (byte*)pluginState.CameraSettings.CameraManagerAddress.ToPointer(), CAM_ZOOM_MIN_ADDR_OFFSET);
                MemoryManager.WriteFloatValue(&cameraZoomMax, (byte*)pluginState.CameraSettings.CameraManagerAddress.ToPointer(), CAM_ZOOM_MAX_ADDR_OFFSET);
                MemoryManager.WriteFloatValue(&cameraUpDown, (byte*)pluginState.CameraSettings.CameraManagerAddress.ToPointer(), CAM_Y_ANGLE_ADDR_OFFSET);
            }
        }

        public void Dispose()
        {
            ResetCameraDefaults();
            refreshTimer.Stop();
            refreshTimer.Dispose();
        }
    }
}
