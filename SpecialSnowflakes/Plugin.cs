﻿using Dalamud.Game.Command;
using Dalamud.IoC;
using Dalamud.Plugin;
using Dalamud.Game;
using Dalamud.Plugin.Services;
using System.Reflection;
using SpecialSnowflakes.Utils;
using System.Linq;

namespace SpecialSnowflakes
{
    public sealed class Plugin : IDalamudPlugin
    {
        public string Name => "Special Snowflakes";

        private const string commandName = "/ssf";

        private IDalamudPluginInterface PluginInterface { get; init; }
        private ICommandManager CommandManager { get; init; }
        private SpecialSnowflakes ssf { get; init; }

        public Plugin(
            IDalamudPluginInterface pluginInterface,
            ICommandManager commandManager,
            IClientState clientState,
            IFramework framework,
            IChatGui chatGui,
            IObjectTable objects,
            ICondition condition,
            IGameInteropProvider gameInteropProvider,
            ISigScanner sigscanner,
            IPluginLog pluginLogger)
        {
            PluginInterface = pluginInterface;
            CommandManager = commandManager;

            Configuration config = PluginInterface.GetPluginConfig() as Configuration ?? new Configuration();
            config.Initialize(pluginInterface);

            ssf = new SpecialSnowflakes(
                clientState,
                condition,
                framework,
                chatGui,
                objects,
                gameInteropProvider,
                sigscanner,
                pluginLogger,
                config
            );

            var assemblyLocation = Assembly.GetExecutingAssembly().Location;

            string[] helpMessageStrings =
            {
                "Open the main window for Special Snowflakes.",
                "/ssf off → Disable all automatic refreshing.",
                "/ssf height [character name] [height] → Temporarily set height for a character nearby.",
                "/ssf always, /ssf on → Enable always-on automatic refreshing.",
                "/ssf interval → Enable automatic refreshing at a time interval. (Set your time in Preferences menu.)",
                "/ssf refresh → Refresh all heights now.",
                "/ssf extras → Open the Extras window.",
                "/ssf camera on, /ssf camera off → Enable or disable dynamic camera.",
                "/ssf fog on, /ssf fog off → Enable or disable environmental fog.",
                "/ssf rlimit on, /ssf rlimit off → Enable or disable character render limit.",
            };

            CommandManager.AddHandler(commandName, new CommandInfo(ssf.OnCommand)
            {
                HelpMessage = helpMessageStrings.Aggregate((a, b) => a + "\n" + b)
            });

            PluginInterface.UiBuilder.OpenMainUi += ssf.DrawMainUI;
            PluginInterface.UiBuilder.Draw += ssf.DrawUI;
            PluginInterface.UiBuilder.OpenConfigUi += ssf.DrawConfigUI;
        }

        public void Dispose()
        {
            ssf.Dispose();
            CommandManager.RemoveHandler(commandName);
            PluginInterface.UiBuilder.Draw -= ssf.DrawUI;
            PluginInterface.UiBuilder.OpenConfigUi -= ssf.DrawConfigUI;
        }

    }
}
