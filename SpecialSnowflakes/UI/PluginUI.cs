﻿using ImGuiNET;
using System.Numerics;
using Dalamud.Logging;
using Dalamud.Interface;
using OtterGui.Raii;

using SpecialSnowflakes.Chara;
using SpecialSnowflakes.Utils;
using FFXIVClientStructs.FFXIV.Client.Graphics.Environment;
using SpecialSnowflakes.Extras;
using FFXIVClientStructs.FFXIV.Client.Game.UI;

namespace SpecialSnowflakes.UI
{
    class PluginUI
    {
        private SpecialSnowflakes pluginController { get; init; }
        private Configuration configuration { get; init; }
        private SSFCharacterManager characterManager { get; init; }
        private CharacterRefreshManager refreshManager { get; init; }

        private Character currentCharacter;
        private string newName = "Player Name";
        private float uiScale = 1f;
        private bool visible = false;
        private bool characterRemoved = false;

        public bool PreferencesVisible = false;
        public bool ExtrasVisible = false;
        public bool Visible
        {
            get { return visible; }
            set { visible = value; }
        }

        public PluginUI(SpecialSnowflakes ssf, Configuration configuration, SSFCharacterManager characterManager, CharacterRefreshManager refreshManager)
        {
            this.pluginController = ssf;
            this.configuration = configuration;
            this.characterManager = characterManager;
            this.refreshManager = refreshManager;
        }

        public void Draw()
        {
            uiScale = Dalamud.Interface.Utility.ImGuiHelpers.GlobalScale;
            DrawMainWindow();
            drawPreferencesWindow();
            drawExtrasWindow();
        }
        private unsafe void addCharacter(string characterName)
        {
            if (characterManager.AddCharacter(characterName))
            {
                SpecialSnowflakes.PluginLogger.Debug($"Found {characterName}!");
                newName = "Player Name";
            }
            else
            {
                SpecialSnowflakes.PluginLogger.Debug($"{characterName} was not added.");
            }
            if (refreshManager.CurrentRefreshState.CurrentMode == RefreshMode.ALWAYS) characterManager.InitQueue();
        }

        private unsafe void removeCharacter(string characterName)
        {
            characterManager.RemoveCharacter(characterName);
            if (refreshManager.CurrentRefreshState.CurrentMode == RefreshMode.ALWAYS) characterManager.InitQueue();
            characterRemoved = true;
        }
        public unsafe void DrawMainWindow()
        {
            if (!visible)
            {
                return;
            }
            ImGui.SetNextWindowSize(new Vector2(500, 500), ImGuiCond.FirstUseEver);
            ImGui.SetNextWindowSizeConstraints(new Vector2(375, 330 * uiScale), new Vector2(float.MaxValue, float.MaxValue));
            if (ImGui.Begin($"Special Snowflakes", ref visible, ImGuiWindowFlags.NoScrollbar | ImGuiWindowFlags.NoScrollWithMouse))
            {
                ImGui.Columns(2);
                {
                    ImGui.BeginChild("CharacterList");
                    ImGui.InputText("", ref newName, 64); ImGui.SameLine();
                    using (var font = ImRaii.PushFont(UiBuilder.IconFont))
                    {
                        if (ImGui.Button(FontAwesomeIcon.Plus.ToIconString(), new Vector2(22f * uiScale, 22f * uiScale)))
                        {
                            addCharacter(newName);
                            configuration.characterList = characterManager.Characters;
                            configuration.Save();
                        }
                    }
                    ImGui.TreePop();
                    foreach (Character character in characterManager.Characters)
                    {
                        var characterListItem = ImGui.TreeNodeEx(character.Name);
                        if (ImGui.IsItemClicked(ImGuiMouseButton.Left))
                        {
                            currentCharacter = character;
                        }
                        if (characterListItem) ImGui.TreePop();
                    }
                    ImGui.EndChild();
                }
                ImGui.SameLine();
                ImGui.NextColumn();
                {
                    ImGui.BeginChild("Character");
                    if (currentCharacter != null)
                    {
                        refreshManager.UpdateCharacterBase(currentCharacter);
                        ImGui.SetCursorPosY(6.0f * uiScale);
                        ImGui.Text(currentCharacter.Name);
                        ImGui.SameLine(ImGui.GetColumnWidth() - 24f * uiScale);
                        ImGui.SetCursorPosY(0f);
                        using (var font = ImRaii.PushFont(UiBuilder.IconFont))
                        {
                            if (ImGui.Button(FontAwesomeIcon.Trash.ToIconString(), new Vector2(24f * uiScale, 24f * uiScale)))
                            {
                                removeCharacter(currentCharacter.Name);
                                configuration.characterList = characterManager.Characters;
                                configuration.Save();
                            }
                        }
                        ImGui.Separator();
                        ImGui.PushItemWidth(ImGui.GetColumnWidth() - 175f * uiScale);
                        float currentCharacterHeight = currentCharacter.Height;
                        if (ImGui.InputFloat("Character Height", ref currentCharacterHeight))
                        {
                            currentCharacter.Height = currentCharacterHeight;
                            configuration.characterList = characterManager.Characters;
                            configuration.Save();
                        }
                        ImGui.PopItemWidth();
                        ImGui.SameLine();
                        if (ImGui.Button("Apply", new Vector2(ImGui.GetColumnWidth(), 24f * uiScale)))
                        {
                            refreshManager.UpdateHeight(currentCharacter);
                            refreshManager.UpdateCameraHeight(currentCharacter);
                        }
                        bool readOnly = currentCharacter.ReadOnlyHeight;
                        if (ImGui.Checkbox("Do not apply on automatic refreshes.", ref readOnly))
                        {
                            currentCharacter.ReadOnlyHeight = readOnly;
                            configuration.characterList = characterManager.Characters;
                            configuration.Save();
                        }
                        if (ImGui.IsItemHovered())
                        {
                            ImGui.BeginTooltip();
                            ImGui.PushTextWrapPos(ImGui.GetFontSize() * 25.0f);
                            ImGui.TextUnformatted("The height for this character will not be applied by automatic refreshes.");
                            ImGui.PushStyleColor(ImGuiCol.Text, new Vector4(255, 255, 0, 1));
                            ImGui.TextUnformatted("Check this if the character is using controlling their height through a TexTool/Penumbra Racial Scaling Edit Mod.");
                            ImGui.PopStyleColor();
                            ImGui.PopTextWrapPos();
                            ImGui.EndTooltip();
                        }
                        bool disableCustomScale = currentCharacter.DisableCustomScale;
                        if (ImGui.Checkbox("Disable custom scale calcuations.", ref disableCustomScale))
                        {
                            if (disableCustomScale == false) currentCharacter.ResetLastKnownPos();
                            currentCharacter.DisableCustomScale = disableCustomScale;
                            configuration.characterList = characterManager.Characters;
                            configuration.Save();
                        }
                        if (ImGui.IsItemHovered())
                        {
                            ImGui.BeginTooltip();
                            ImGui.PushTextWrapPos(ImGui.GetFontSize() * 25.0f);
                            ImGui.TextUnformatted("Scaling modifcations to the root bone will not be used in calculations.");
                            ImGui.PushStyleColor(ImGuiCol.Text, new Vector4(255, 255, 0, 1));
                            ImGui.TextUnformatted("Check this if the character is controlling their height through Customize+, but does not need extra calculations for features in Extras.");
                            ImGui.PopStyleColor();
                            ImGui.PopTextWrapPos();
                            ImGui.EndTooltip();
                        }
                        byte* characterBase = currentCharacter.GetLastKnownCharacterBase();
                        byte* modelView = currentCharacter.GetLastKnownModelView();
                        if (characterBase != null && modelView != null)
                        {
                            float currentHeight = refreshManager.GetCurrentHeight(modelView);
                            float currentScale = refreshManager.GetCustomScale(modelView);
                            if (currentHeight > 0)
                            {
                                ImGui.SetCursorPosY(ImGui.GetWindowHeight() - 95 * uiScale);
                                ImGui.Text("Current Height: " + currentHeight);
                                if (!disableCustomScale)
                                {
                                    ImGui.Text("Current Custom Scale: " + currentScale);
                                    ImGui.Text("True Height: " + currentHeight * currentScale);
                                }
                            }
                        }
                        else
                        {
                            refreshManager.UpdateCharacterBase(currentCharacter);
                        }
                    }
                    ImGui.EndChild();
                }
            }
            ImGui.SetCursorPosY(ImGui.GetWindowHeight() - 40 * uiScale);
            ImGui.Columns(1);
            ImGui.BeginChild("Bottom Configuration");
            ImGui.Separator();
            if (ImGui.Button("Refresh All Characters", new Vector2(150f * uiScale, 24f * uiScale)))
            {
                refreshManager.RefreshAllHeights();
            }

            string refreshStateText = $"Automatic refreshing is {(refreshManager.CurrentRefreshState.CurrentMode == RefreshMode.DISABLED ? "disabled." : "active.")}";
            ImGui.SameLine();
            ImGui.SetCursorPosY(6.0f * uiScale);
            ImGui.SetCursorPosX((ImGui.GetWindowWidth() - ImGui.CalcTextSize(refreshStateText).X) / 2.0f);
            ImGui.TextUnformatted(refreshStateText);
            if (ImGui.IsItemHovered())
            {
                ImGui.BeginTooltip();
                ImGui.PushTextWrapPos(ImGui.GetFontSize() * 25.0f);
                if (refreshManager.CurrentRefreshState.CurrentMode == RefreshMode.DISABLED)
                {
                    ImGui.TextUnformatted("The main functionality of this plugin is not active unless manually updated.");
                } else
                {
                    ImGui.TextUnformatted("Functionality related to character heights are active.");
                }
                ImGui.PopTextWrapPos();
                ImGui.EndTooltip();
            }
            ImGui.SameLine(ImGui.GetWindowWidth() - 80f * uiScale);
            if (ImGui.Button("Extras", new Vector2(50f * uiScale, 24f * uiScale)))
            {
                ExtrasVisible =! ExtrasVisible;
            }
            if (ImGui.IsItemHovered())
            {
                ImGui.BeginTooltip();
                ImGui.PushStyleColor(ImGuiCol.Text, new Vector4(255, 0, 0, 1));
                ImGui.PushTextWrapPos(ImGui.GetFontSize() * 25.0f);
                ImGui.TextUnformatted("These extra adjustments can provide an unfair advantage to players and is certainly against FINAL FANTASY XIV and SQUARE ENIX Terms of Service. Use at your own discretion.");
                ImGui.PopTextWrapPos();
                ImGui.PopStyleColor();
                ImGui.EndTooltip();
            }
            ImGui.SameLine(ImGui.GetWindowWidth() - 24f * uiScale);
            using (var font = ImRaii.PushFont(UiBuilder.IconFont))
            {
                if (ImGui.Button(FontAwesomeIcon.Cog.ToIconString(), new Vector2(24f * uiScale, 24f * uiScale)))
                {
                    PreferencesVisible = ! PreferencesVisible;
                }
            }
            ImGui.EndChild();
            ImGui.End();
            if (characterRemoved)
            {
                currentCharacter = null;
                characterRemoved = false;
            }
        }

        public void stopRefreshCommand()
        {
            refreshManager.StopRefresh();
            configuration.refreshBoolOption = refreshManager.CurrentRefreshState.CurrentMode;
            configuration.Save();
        }

        public void drawPreferencesWindow()
        {
            if (!PreferencesVisible)
            {
                return;
            }
            ImGui.SetNextWindowSize(new Vector2(300 * uiScale, 140 * uiScale), ImGuiCond.Always);
            if (ImGui.Begin("Special Snowflakes Preferences", ref PreferencesVisible,
               ImGuiWindowFlags.NoResize | ImGuiWindowFlags.NoCollapse | ImGuiWindowFlags.NoScrollbar | ImGuiWindowFlags.NoScrollWithMouse))
            {
                ImGui.Text("Automatic Refresh Settings");
                int refreshRadioButton = (int)refreshManager.CurrentRefreshState.CurrentMode;
                if (ImGui.RadioButton("Disable", ref refreshRadioButton, (int)RefreshMode.DISABLED))
                {
                    refreshManager.CurrentRefreshState.CurrentMode = (RefreshMode)refreshRadioButton;
                    configuration.refreshBoolOption = refreshManager.CurrentRefreshState.CurrentMode;
                    configuration.Save();
                    stopRefreshCommand();
                }
                if (ImGui.RadioButton("Always refresh", ref refreshRadioButton, (int)RefreshMode.ALWAYS))
                {
                    refreshManager.CurrentRefreshState.CurrentMode = (RefreshMode)refreshRadioButton;
                    configuration.refreshBoolOption = refreshManager.CurrentRefreshState.CurrentMode;
                    configuration.Save();
                    characterManager.InitQueue();
                    stopRefreshCommand();
                }
                var refreshBoolUpdate = ImGui.RadioButton("Refresh every", ref refreshRadioButton, (int)RefreshMode.INTERVAL);
                int refreshInterval = refreshManager.CurrentRefreshState.IntervalRate;
                ImGui.SameLine();
                if (refreshBoolUpdate)
                {
                    refreshManager.CurrentRefreshState.CurrentMode = (RefreshMode)refreshRadioButton;
                    refreshManager.CurrentRefreshState.IntervalRate = refreshInterval;
                    configuration.refreshBoolOption = refreshManager.CurrentRefreshState.CurrentMode;
                    configuration.Save();
                    stopRefreshCommand();
                }
                ImGui.PushItemWidth(100.0f * uiScale);
                var refreshIntervalUpdate = ImGui.InputInt("seconds", ref refreshInterval);
                if (refreshIntervalUpdate)
                {
                    refreshManager.CurrentRefreshState.IntervalRate = refreshInterval;
                    refreshManager.CurrentRefreshState.CurrentMode = (RefreshMode)refreshRadioButton;
                    if (refreshRadioButton == (int)RefreshMode.INTERVAL) refreshManager.RestartRefresh();
                    configuration.refreshIntervallUpdate = refreshInterval;
                    configuration.Save();
                }
            }
            ImGui.End();
        }

        public void drawExtrasWindow()
        {
            if (!ExtrasVisible)
            {
                return;
            }
            ImGui.SetNextWindowSize(new Vector2(360 * uiScale, 420 * uiScale), ImGuiCond.FirstUseEver);
            ImGui.SetNextWindowSizeConstraints(new Vector2(300 * uiScale, 140 * uiScale), new Vector2(float.MaxValue, float.MaxValue));
            if (ImGui.Begin("Special Snowflakes Extras", ref ExtrasVisible))
            {
                drawCameraControl();
                drawFogControl();
                drawRenderControl();
                drawExtraOptions();
            }
            ImGui.End();
        }

        private void drawCameraControl()
        {
            if (ImGui.CollapsingHeader("Dynamic Camera Control"))
            {
                bool zoomAdjustments = PluginPreferences.AllowZoomAdjustments;
                if (ImGui.Checkbox("Enable dynamic camera scaling.", ref zoomAdjustments))
                {
                    PluginPreferences.AllowZoomAdjustments = zoomAdjustments;
                    configuration.zoomScaling = zoomAdjustments;
                    configuration.Save();
                    refreshManager.ResetCameraDefaults();
                }
                if (ImGui.IsItemHovered())
                {
                    ImGui.BeginTooltip();
                    ImGui.PushTextWrapPos(ImGui.GetFontSize() * 25.0f);
                    ImGui.PushStyleColor(ImGuiCol.Text, new Vector4(255, 255, 0, 1));
                    ImGui.TextUnformatted("The game's default will set your camera height to your character height. Use a plugin like Customize+ for height scaling if this is undesired.");
                    ImGui.PopStyleColor();
                    ImGui.PopTextWrapPos();
                    ImGui.EndTooltip();
                }
                if (!zoomAdjustments) ImGui.BeginDisabled();
                ImGui.Indent(30f * uiScale);
                bool allowDutyDynamicZoom = PluginPreferences.DisllowDutyZoomAdjustments;
                if (ImGui.Checkbox("Disable during combat or duties.", ref allowDutyDynamicZoom))
                {
                    PluginPreferences.DisllowDutyZoomAdjustments = allowDutyDynamicZoom;
                    configuration.allowDutyDynamicZoom = allowDutyDynamicZoom;
                    configuration.Save();
                }
                bool allowMoutnedDynamicZoom = PluginPreferences.DisallowMountedZoomAdjustments;
                if (ImGui.Checkbox("Disable while mounted.", ref allowMoutnedDynamicZoom))
                {
                    PluginPreferences.DisallowMountedZoomAdjustments = allowMoutnedDynamicZoom;
                    configuration.disableMountDynamicZoom = allowMoutnedDynamicZoom;
                    configuration.Save();
                }
                bool allowGPoseDynamicZoom = PluginPreferences.DisallowGPoseZoomAdjustments;
                if (ImGui.Checkbox("Disable during Group Pose.", ref allowGPoseDynamicZoom))
                {
                    PluginPreferences.DisallowGPoseZoomAdjustments = allowGPoseDynamicZoom;
                    configuration.disableGPoseDynamicZoom = allowGPoseDynamicZoom;
                    configuration.Save();
                }
                ImGui.Indent(-30f * uiScale);
                if (!zoomAdjustments) ImGui.EndDisabled();
            }
        }

        private void drawFogControl()
        {
            if (ImGui.CollapsingHeader("Fog Control"))
            {
                ImGui.PushStyleColor(ImGuiCol.Text, new Vector4(1, 0.5f, 0, 1));
                ImGui.PushTextWrapPos(ImGui.GetWindowWidth());
                ImGui.Text("May conflict with Ktisis's Advanced Environment Editor (Testing Branch).");
                ImGui.PopTextWrapPos();
                ImGui.PopStyleColor();
                bool fogOverride = PluginPreferences.FogOverride;
                if (ImGui.Checkbox("Override environmental fog + lighting.", ref fogOverride))
                {
                    if (fogOverride)
                    {
                        pluginController.FogRemoval.disableFog();
                    }
                    else
                    {
                        pluginController.FogRemoval.enableFog();
                    }
                }
                if (!fogOverride) ImGui.BeginDisabled();
                else unsafe
                    {
                        var environmentState = &EnvironmentParameters.Instance()->EnvState;
                        ImGui.Indent(30f * uiScale);
                        ImGui.PushStyleColor(ImGuiCol.Text, new Vector4(1, 1, 0, 1));
                        ImGui.PushTextWrapPos(ImGui.GetWindowWidth() - 30f * uiScale);
                        ImGui.Text("Environmental overrides may change when entering new regions of a zone.");
                        ImGui.PopTextWrapPos();
                        ImGui.PopStyleColor();
                        ImGui.Text("Fog Settings");
                        ImGui.ColorEdit4("Color", ref environmentState->Fog.Color);
                        ImGui.SliderFloat("Distance", ref environmentState->Fog.Distance, 0f, 1000f);
                        ImGui.SliderFloat("Thickness", ref environmentState->Fog.Thickness, 0f, 100f);
                        ImGui.SliderFloat("Opacity", ref environmentState->Fog.Opacity, 0f, 1f);
                        ImGui.SliderFloat("Sky Visibility", ref environmentState->Fog.SkyVisibility, 0f, 1f);
                        ImGui.SliderFloat("Skybox Distance", ref environmentState->Fog._unk1, 0f, 3000f);
                        ImGui.SliderFloat("Skybox Mix", ref environmentState->Fog._unk2, 0f, 1f);

                        ImGui.Spacing();

                        ImGui.Text("Lighting Settings");
                        ImGui.ColorEdit3("Sunlight", ref environmentState->Lighting.SunLightColor);
                        ImGui.ColorEdit3("Moonlight", ref environmentState->Lighting.MoonLightColor);
                        ImGui.ColorEdit3("Ambient", ref environmentState->Lighting.Ambient);
                        ImGui.SliderFloat("Saturation", ref environmentState->Lighting.AmbientSaturation, 0.0f, 5.0f);
                        ImGui.SliderFloat("Temperature", ref environmentState->Lighting.Temperature, -2.5f, 2.5f);
                        ImGui.SliderFloat("Hue Shift", ref environmentState->Lighting._unk2, 0.0f, 1.0f);
                        ImGui.SliderFloat("Light Thickness", ref environmentState->Lighting._unk3, 0.0f, 200.0f);
                        ImGui.SliderFloat("Light Distance", ref environmentState->Lighting._unk4, 0.0f, 1000.0f);
                        ImGui.SliderFloat("Unknown Parameter", ref environmentState->Lighting._unk1, 0.0f, 10.0f);

                        ImGui.Spacing();

                        ImGui.PushStyleColor(ImGuiCol.Text, new Vector4(0.5f, 0.5f, 0.5f, 1));
                        ImGui.PushTextWrapPos(ImGui.GetWindowWidth() - 30f * uiScale);
                        ImGui.Text("Tip: Hold CTRL + Click to manually input values.");
                        ImGui.PopTextWrapPos();
                        ImGui.PopStyleColor();
                        ImGui.Indent(-30f * uiScale);
                    }
                if (!fogOverride) ImGui.EndDisabled();
            }
        }

        private void drawRenderControl()
        {
            if (ImGui.CollapsingHeader("Character Render Control"))
            {
                bool characterDelimit = PluginPreferences.CharacterDelimit;
                if (ImGui.Checkbox("Disable character rendering scale limit.", ref characterDelimit))
                {
                    if (characterDelimit)
                    {
                        pluginController.CharacterDelimit.disableLimit();
                    }
                    else
                    {
                        pluginController.CharacterDelimit.enableLimit();
                    }
                }
            }
        }

        private void drawExtraOptions()
        {
            if (ImGui.CollapsingHeader("Misc. Options"))
            {
                bool swapExtrasMain = PluginPreferences.SwapExtrasAndMain;
                if (ImGui.Checkbox("Default /ssf to open SSF Extras.", ref swapExtrasMain))
                {
                    PluginPreferences.SwapExtrasAndMain = swapExtrasMain;
                    configuration.swapExtrasMain = swapExtrasMain;
                    configuration.Save();
                }
            }
        }
    }
}
